hint "Spartan Elements, this is Overlord 6. We are dispatching Kingmaker 1 to your location, ETA 5-8 minutes. Be advised we are picking up a lot of enemy movement in route to your general vicinity.";

_group1 = [getMarkerPos "extract_phase_spawn", EAST, (configfile >> "CfgGroups" >> "EAST" >> "caf_ag_eeur_r" >> "Infantry" >> "10_men_eeur_r"),[],[],[.2],[],180] call BIS_fnc_spawnGroup;
_group2 = [getMarkerPos "extract_phase_spawn", EAST, (configfile >> "CfgGroups" >> "EAST" >> "caf_ag_eeur_r" >> "Infantry" >> "10_men_eeur_r"),[],[],[.2],[],180] call BIS_fnc_spawnGroup;
_group3 = [getMarkerPos "extract_phase_spawn", EAST, (configfile >> "CfgGroups" >> "EAST" >> "caf_ag_eeur_r" >> "Infantry" >> "10_men_eeur_r"),[],[],[.2],[],180] call BIS_fnc_spawnGroup;
_group4 = [getMarkerPos "extract_phase_spawn", EAST, (configfile >> "CfgGroups" >> "EAST" >> "caf_ag_eeur_r" >> "Infantry" >> "10_men_eeur_r"),[],[],[.2],[],180] call BIS_fnc_spawnGroup;

// Add to Curator's control
rodriguez addCuratorEditableObjects [units _group1,false];
rodriguez addCuratorEditableObjects [units _group2,false];
rodriguez addCuratorEditableObjects [units _group3,false];
rodriguez addCuratorEditableObjects [units _group4,false];