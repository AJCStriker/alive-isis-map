//////////////////////////////////////
//----ALiVE NATOFOR Random Tasks----//
//---By Valixx, Kylania & M4RT14L---//
//---------------v1.4---------------//
//////////////////////////////////////

_missionType = [_this, 0, ""] call BIS_fnc_param;

sleep 240;

_markerArray = ["town","town_1","town_2","town_3","town_4","town_5","town_6","town_7","town_8","town_9","town_10","town_11","town_12","town_13","town_14"];
_rnd 	= floor (random (count(_markerArray)));
_mrkSpawnTown = getMarkerPos (_markerArray select _rnd);

sleep 60;

fn_spawnClearMission = {

	hint "COMBAT OPS UPDATED";
	//creating the marker 

	_markerCO = createMarker ["mob_clear", _mrkSpawnTown];
	_markerCO setMarkerType "mil_warning";
	_markerCO setMarkerColor "ColorRed";
	_markerCO setMarkerText "CLEAR OP";
	_markerCO setMarkerSize [1,1];
	
	_null = [west, "mob_clear", ["Take control of the zone and drive out OPFOR forces.", "Area Clear", "Area Clear"], getMarkerPos "mob_clear", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_clear", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerCO, 0, 200, 10, 0, 20, 0] call BIS_fnc_findSafePos;
	
	hq = createVehicle ["O_Truck_03_covered_F",[(getMarkerpos _markerCO select 0) + 30, getMarkerpos _markerCO select 1,0],[], 0, "NONE"];
	
	ifv1 = createGroup EAST;
	[_newPos, 10, "O_APC_Wheeled_02_rcws_F", ifv1] call BIS_fnc_spawnvehicle;
	nul = [ifv1,getpos hq, 300] call BIS_fnc_taskPatrol;
	sleep 10;
	[_newPos, 10, "O_APC_Wheeled_02_rcws_F", ifv1] call BIS_fnc_spawnvehicle;
	sleep 10;
	[_newPos, 10, "O_APC_Wheeled_02_rcws_F", ifv1] call BIS_fnc_spawnvehicle;
	
	grp1C = [getMarkerPos _markerCO, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
	nul = [grp1C,getpos hq, 150] call BIS_fnc_taskPatrol;
	
	grp2C = [getMarkerPos _markerCO, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
	nul = [grp2C,getpos hq, 150] call BIS_fnc_taskPatrol;
	
	grp3C = [getMarkerPos _markerCO, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
	nul = [grp3C,getpos hq, 150] call BIS_fnc_taskPatrol;

	_trg = createTrigger ["EmptyDetector", getMarkerPos _markerCO]; 
	_trg setTriggerArea [300, 300, 0, false]; 
	_trg setTriggerActivation ["EAST", "NOT PRESENT", false]; 
	_trg setTriggerStatements ["this", "", ""]; 

	enemyDead = false; 
	waitUntil {triggerActivated _trg}; 

	_null = ["mob_clear", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerCO;
	
	{deleteVehicle _x} forEach units grp1C;
	{deleteVehicle _x} forEach units grp2C;
	{deleteVehicle _x} forEach units grp3C;
	{deleteVehicle _x} forEach units ifv1;
	deleteGroup ifv1;
	deleteGroup grp1C;
	deleteGroup grp2C;
	deleteGroup grp3C;
	deleteVehicle hq;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_clear"] call LARs_fnc_removeTask;

	sleep 500;
};

fn_spawnKillMission = {

	hint "COMBAT OPS UPDATED";
	//creating the marker 

	_markerCO = createMarker ["mob_kill", _mrkSpawnTown];
	_markerCO setMarkerType "mil_warning";
	_markerCO setMarkerColor "ColorRed";
	_markerCO setMarkerText "CLEAR OP";
	_markerCO setMarkerSize [1,1];
	
	_null = [west, "mob_kill", ["Kill the enemy officers", "Kill Officers", "Kill Officers"], getMarkerPos "mob_kill", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_kill", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	mrap = createVehicle ["O_MRAP_02_F",[(getMarkerpos _markerCO select 0) + 3, getMarkerpos _markerCO select 1,0],[], 0, "NONE"];
	
	_off = createGroup EAST;
	officer1 = _off createUnit ["O_officer_F",[(getMarkerpos _markerCO select 0) + 1, getMarkerpos _markerCO select 1,0], [], 0, "NONE"];
	officer2 = _off createUnit ["O_officer_F",[(getMarkerpos _markerCO select 0) + 2, getMarkerpos _markerCO select 1,0], [], 0, "NONE"];
	
	grp1C = [getMarkerPos _markerCO, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
	nul = [grp1C,getpos mrap, 100] call BIS_fnc_taskPatrol;
	
	grp2C = [getMarkerPos _markerCO, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
	nul = [grp2C,getpos mrap, 100] call BIS_fnc_taskPatrol;

	waitUntil {!alive officer1 && !alive officer2};
	
	_null = ["mob_kill", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerCO;
	
	{deleteVehicle _x} forEach units grp1C;
	{deleteVehicle _x} forEach units grp2C;
	deleteGroup grp1C;
	deleteGroup grp2C;
	deleteGroup _off;
	deleteVehicle mrap;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_kill"] call LARs_fnc_removeTask;

	sleep 500;
};

fn_spawnAmmoMission = {

	hint "COMBAT OPS UPDATED";
	//creating the marker 

	_markerCO = createMarker ["mob_ammo", _mrkSpawnTown];
	_markerCO setMarkerType "mil_warning";
	_markerCO setMarkerColor "ColorRed";
	_markerCO setMarkerText "CLEAR OP";
	_markerCO setMarkerSize [1,1];
	
	_null = [west, "mob_ammo", ["Eliminate the supply truck.", "Destroy Supplies", "Destroy Supplies"], getMarkerPos "mob_ammo", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_ammo", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerCO, 0, 100, 10, 0, 20, 0] call BIS_fnc_findSafePos;

	_supply = ["O_Truck_02_fuel_F","O_Truck_02_Ammo_F","O_Truck_02_box_F"] call BIS_fnc_selectRandom;
	
	supplies = createVehicle [_supply, _newPos, [], 0, "NONE"];
	
	grp1C = [getMarkerPos _markerCO, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
	nul = [grp1C,getpos supplies, 300] call BIS_fnc_taskPatrol;
	
	grp2C = [getMarkerPos _markerCO, EAST, (configfile >> "CfgGroups" >> "East" >> "OPF_F" >> "Infantry" >> "OIA_InfSquad")] call BIS_fnc_spawnGroup;
	nul = [grp2C,getpos supplies, 300] call BIS_fnc_taskPatrol;

	waitUntil {!alive supplies};
	
	_null = ["mob_ammo", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerCO;
	
	{deleteVehicle _x} forEach units grp1C;
	{deleteVehicle _x} forEach units grp2C;
	deleteGroup grp1C;
	deleteGroup grp2C;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_ammo"] call LARs_fnc_removeTask;

	sleep 500;
};

fn_spawnAmmo2Mission = {

	hint "COMBAT OPS UPDATED";
	//creating the marker 

	_markerCO = createMarker ["mob_ammo2", _mrkSpawnTown];
	_markerCO setMarkerType "mil_warning";
	_markerCO setMarkerColor "ColorRed";
	_markerCO setMarkerText "CLEAR OP";
	_markerCO setMarkerSize [1,1];
	
	_null = [west, "mob_ammo2", ["Find and destroy the ammo cache.", "Destroy Cache", "Destruoy Cache"], getMarkerPos "mob_ammo2", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_ammo2", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerCO, 0, 75, 10, 0, 20, 0] call BIS_fnc_findSafePos;

	_box = ["Box_East_AmmoVeh_F","Box_IND_Wps_F","Box_IND_WpsSpecial_F","Box_IND_WpsLaunch_F","Box_IND_AmmoOrd_F"] call BIS_fnc_selectRandom;
	
	cache = createVehicle [_box, _newPos, [], 0, "NONE"];

	waitUntil {!alive cache};
	
	_null = ["mob_ammo2", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerCO;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_ammo2"] call LARs_fnc_removeTask;

	sleep 500;
};

fn_spawnComsMission = {

	hint "COMBAT OPS UPDATED";
	//creating the marker 

	_markerCO = createMarker ["mob_comms", _mrkSpawnTown];
	_markerCO setMarkerType "mil_warning";
	_markerCO setMarkerColor "ColorRed";
	_markerCO setMarkerText "CLEAR OP";
	_markerCO setMarkerSize [1,1];
	
	_null = [west, "mob_comms", ["Find and destroy OPFOR mobile comms", "Destroy Comms", "Destroy Comms"], getMarkerPos "mob_comms", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_comms", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerCO, 0, 75, 10, 0, 20, 0] call BIS_fnc_findSafePos;

	_comms = ["Land_TTowerSmall_1_F","Land_TTowerSmall_2_F"] call BIS_fnc_selectRandom;
	
	rtower = createVehicle [_comms, _newPos, [], 0, "NONE"];
	movilhq = createVehicle ["O_Truck_03_device_F",[(getPos rtower select 0) + 5, getPos rtower select 1,0],[], 0, "NONE"];
	camonet = createVehicle ["CamoNet_OPFOR_big_F", getPos movilhq, [], 0, "CAN_COLLIDE"];

	waitUntil {!alive rtower};
	
	_null = ["mob_comms", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerCO;
	deleteVehicle movilhq;
	deleteVehicle camonet;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLATED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_comms"] call LARs_fnc_removeTask;

	sleep 500;
};

fn_spawnArmorMission = {

	hint "COMBAT OPS UPDATED";
	//creating the marker 

	_markerCO = createMarker ["mob_armor", _mrkSpawnTown];
	_markerCO setMarkerType "mil_warning";
	_markerCO setMarkerColor "ColorRed";
	_markerCO setMarkerText "CLEAR OP";
	_markerCO setMarkerSize [1,1];
	
	_null = [west, "mob_armor", ["Destroy enemy armor group", "Destroy Armor", "Destroy Armor"], getMarkerPos "mob_armor", false] spawn BIS_fnc_taskCreate;
	_null = ["mob_armor", "CREATED"] spawn BIS_fnc_taskSetState;
	
	sleep 30;

	//creating the vehicle
	
	_newPos = [getMarkerPos _markerCO, 0, 50, 10, 0, 20, 0] call BIS_fnc_findSafePos;
	
	tanques1 = createGroup EAST;
	[_newPos, 10, "O_MBT_02_cannon_F", tanques1] call BIS_fnc_spawnvehicle;
	nul = [tanques1,getMarkerPos _markerCO, 200] call BIS_fnc_taskPatrol;
	sleep 10;
	[_newPos, 10, "O_MBT_02_cannon_F", tanques1] call BIS_fnc_spawnvehicle;
	sleep 10;
	[_newPos, 10, "O_MBT_02_cannon_F", tanques1] call BIS_fnc_spawnvehicle;
	sleep 10;
	[_newPos, 10, "O_APC_Tracked_02_cannon_F", tanques1] call BIS_fnc_spawnvehicle;
	sleep 10;
	[_newPos, 10, "O_APC_Tracked_02_cannon_F", tanques1] call BIS_fnc_spawnvehicle;
	
	waitUntil {{ alive _x } count units tanques1 == 0}; 

	_null = ["mob_armor", "SUCCEEDED"] spawn BIS_fnc_taskSetState;
	
	sleep 100;

	deleteMarker _markerCO;
	{deleteVehicle _x} forEach units tanques1;
	deleteGroup tanques1;
	deleteVehicle armorhq;

	_myHint ="Good Job!";
	GlobalHint = _myHint;
	publicVariable "GlobalHint";
	hintsilent parseText _myHint;

	_mySChat ="OBJECTIVE COMPLETED";
	GlobalSCHat = _mySChat;
	publicVariable "GlobalSCHat";
	player sideChat _mySChat;
	
	[west, "mob_armor"] call LARs_fnc_removeTask;

	sleep 500;
};

// MAIN LOGIC

_missionDetails = switch (_missionType) do {
	case "clear": {call fn_spawnClearMission;};
	case "kill": {call fn_spawnKillMission;};
	case "ammo": {call fn_spawnAmmoMission;};
	case "ammo2": {call fn_spawnAmmo2Mission;};
	case "comms": {call fn_spawnComsMission;};
	case "armor": {call fn_spawnArmorMission;};
};	

nul = [] execVM "scripts\missionClear.sqf";